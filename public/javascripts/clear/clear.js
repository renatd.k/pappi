
$(document).ready(function(){

showCatalog();
$('.content ul').on('click', 'li', showSubcatalogs);

});

function showCatalog(){
  $.getJSON('/catalogs/cataloglist', function (data){
    
    var listcatalog = "";

    $.each(data, function(){
        listcatalog += '<li id='+this._id+'><img src="/images/noimage.jpg" width="70px" /><span >'+this.name+'</span></li>';
      });

      // Inject the whole content string into our existing HTML table
      $('.content ul').html(listcatalog);
    });
};

//Отображение продуктов
function showSubcatalogs(event){
  var subcatalogContent = "";
  var catalogid ="";
  catalogid = $(this).attr("id");
  
  $.getJSON('/catalogs/subcatalog/'+catalogid, function (data){
    $.each(data, function(){
        subcatalogContent += '<li id='+this._id+'><img src="/images/noimage.jpg" width="70px" /><span >'+this.name+'</span></li>';
    });
   $('.content ul').html(subcatalogContent);
   $('.footer').html($('.footer').html());
  });
};


