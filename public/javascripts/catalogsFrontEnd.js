//Catalog list data in array
var catalogListData = []
var subcatalogListData = []
var productListData = []
var subproductListData = []

var Cart = TAFFY();

var taffyCatalogs = TAFFY(); 
var taffySubcatalgos = TAFFY(); 

var taffyProducts = TAFFY(); 
var taffySubproducts = TAFFY(); 

var subcatalogName = "";
var subcatalogId = "";
// DOM Ready ==============================
$(document).ready(function(){
  
   //Отображаем список каталога
  Cart.store("cookieCart");
  taffyProducts.store("cookieProducts");
  taffySubproducts.store("cookieSubproducts");
  taffyCatalogs.store("cookieCatalogs");

  getAllCatalogs();


  //Отображаем продукты из подкаталога
  $('#menuContent ul').on('click','li a.linkshowproducts', showProducts);
  $('#listProducts ul').on('click','a.linkshowproducts', showProducts);

  $('#listProducts ul').on('click','li a.showSubproduct', showSubproducts);

  showCart();

  $('#listProducts ul').on('click','li div.btnAddToCart', addToCart);

  getAllProducts();
  getAllSubproducts();
 });



// Functions ==============================
function getAllCatalogs(){
  $.getJSON('/catalogs/cataloglist', function (data){
    taffyCatalogs = TAFFY(data);
    showMenuCatalog();
  });
};

function getAllSubcatalogs(){
  $.getJSON('/catalogs/subcataloglist', function (data){
    taffySubcatalogs = TAFFY(data);
  });
};

function getAllProducts(){
  $.getJSON('/catalogs/productlist', function (data){
    taffyProducts = TAFFY(data);
  });
};
function getAllSubproducts(){
  $.getJSON('/subproducts/all', function (data){
    taffySubproducts = TAFFY(data);
  });
};

// Загружаем из базы список каталога и выводим в меню
function showMenuCatalog() {
  
  var menuContent = "";

  //Загрузки подкаталогов
  $.getJSON('/catalogs/subcataloglist', function (data){
      subcatalogListData = data;

    //Загрузка каталогов
    // $.getJSON( '/catalogs/cataloglist', function (data){

      // Присваиваем массив Каталога в глобальную переменнуб catalogListData
      // catalogListData = data;

      // $.each(data, function(){
      taffyCatalogs().each(function(item){
        menuContent += '<li id='+item._id+'>';
        menuContent += '<span class="menuHeder" >'+item.name+'</span>';
        menuContent += '<ul>';
        
        var catalogId = item._id;
        $.each(subcatalogListData, function(){
          if (catalogId === this.catalogs){
            menuContent +='<li><a href="#" id="'+ this._id +'" class="linkshowproducts" >'+this.name+'</a></li>'
          }
        });

        menuContent += '</ul>';
        menuContent += '</li>';
      });

      $('#menuContent ul').html(menuContent);
    // });
  });
};

//Отображение продуктов
function showProducts(event){

  event.preventDefault();

  var productContent = "<a href='/'>Главная</a> > " +$(this).text()+"<br><br>";

  subcatalogId = $(this).attr('id');
  subcatalogName = $(this).text();
  var getProductList=taffyProducts({"subcatalog":subcatalogId});
    getProductList.each(function(data){
        
        var getSubproductList=taffySubproducts({"product":data._id,"spcount":{gt:0}});
        if(getSubproductList.count()>0){
          productContent += "<li class='robotofont'>"+data.name;
          productContent += '<center><a href="#" name='+data.name+' id='+ data._id +' class="showSubproduct "><img class="productImage" src="'+data.image+'"></a>';
          productContent += "</center></li>";  
        };
    });
  $('#listProducts ul').html(productContent);
  $('html, body').animate({scrollTop: '0px'}, 1000);
};

//Отображение попродуктов
function showSubproducts(event){
  
  event.preventDefault();

  var subproductContent = "<a href='/'>Главная</a> >  <a href='#' id="+subcatalogId+" class='linkshowproducts' >"+ subcatalogName + "</a> > "+$(this).attr('name')+"<br><br>";

  var productId = $(this).attr('id');
  var getSubproductList=taffySubproducts({"product":productId,"spcount":{gt:0}});

    getSubproductList.each(function(data){
                subproductContent +="<li class='listSubproduct'><span class='robotofont'>"+data.name+"</span><br />";
                subproductContent +="<img src='"+data.image+"'>"+"<br /><br /><br />";
                subproductContent += "<span class='robotofont'>На складе: "+data.spcount+"</span>";
                subproductContent += "<span class='robotofont right'>Цена: "+data.price_our+"тг</span><br/><br/>";
                subproductContent += "<span class='robotofont'>Вес: "+data.wt+"</span><br/><br/>";
                subproductContent += "<div class='btnAddToCart robotofont red' id="+data._id+"><a href='#'>Отправить в Корзину<a/></div></li>";
    });
    $('#listProducts ul').html(subproductContent);
    $('html, body').animate({scrollTop: '0px'}, 1000);

};

// Добавление продуктов в корзину
function addToCart(event){

 event.preventDefault(); //Отключаем возможность ссылки работать как она работает, т.е. переход по ссылке.

 var SPId = $(this).attr('id');
 var getURL = '/subproducts/'+SPId;
 
 $.getJSON(getURL, function (data){
    
    if (Cart(data).count()>0){
        Cart(data).update(function(){
            this.cartCount++;
            this.sum=this.cartCount*this.price_our;
            return this;
          });
      }
      else{
            Cart.insert(data);
            Cart(data).update({cartCount:1});
            Cart(data).update({sum:data.price_our});
      }
   showCart();
 });

//Анимация добавления в корзину

   var basket = $('#basket');
   var imgDrag = $(this).parent('.listSubproduct').find("img").eq(0);
   if (imgDrag){
    var imgClone = imgDrag.clone()
        .offset({
          top: imgDrag.offset().top,
          left: imgDrag.offset().left
        })
        .css({
          'opacity':'0.7',
          'position':'absolute',
          'height':'150px',
          'wigth':'150px',
          'z-index':'100'
        })
        .appendTo($('body'))
        .animate({
          'top':basket.offset().top+10,
          'left':basket.offset().left+10,
          'height':75,
          'wigth': 75
        },1000, 'easeInElastic');

        imgClone.animate({
          'wigth':0,
          'height':0,
        },function(){
          $(this).detach();
        });
    }; 


};

function showCart(){
 productContent = '';

  Cart().each(function(record, recordnumber){
    productContent +=  "<tr><td><img height='35px' align='left' src='"+record.image+"' /></td><td> "+ record.name+"<br> Количество: <b>"+record.cartCount+"шт</b> </td></tr>";
  });
 $('#basket span').text(Cart().count());
 $('#basket table').html(productContent);

 if (productContent === ''){
  $('#basket ul').text('Корзина пуста');
 }
};

function clearCart(event){
  event.preventDefault();

  list = cookieList("CartList");
  list.clear;

  $('#Cart').text('');  
};

