var SPScipList = 0;

// DOM Ready =================================
$(document).ready(function(){
	showSP();
    showSelectProducts();

	$('#btnAddSP').on('click', addSP);
	$('#btnUpdateSP').on('click', updateSP);

	$('#List table tbody').on('click' , 'td a.linkdeleteSP', deleteSP);
	$('#List table tbody').on('click', 'td a.linkshowSP', showSPInfo);

	$('#btnSerchSP').on('click', searchSP);

	$('#incSkip').on('click',IncSkip);
	$('#decSkip').on('click',decSkip);
});

$(document).keypress(function(e) {
    if(e.which == 13) {
		searchSP();
    };
});

function showSP(){
//Empty content string
	var tableContent = '';

	//jQuery AJAX call for JSON
	$.getJSON( '/subproducts/list/'+SPScipList, function( data ) {

 		// Stick our user data array into a userlist variable in the global object
        userListData = data;
		
		//For each item in our JSON, add a table row and cells to the content string
		$.each(data, function(){
			tableContent += '<tr>';
			tableContent += '<td><a href="#" class="linkshowSP" id='+this._id+' rel="' + this.name + '" title="Show Details">' + this.name + '</a></td>';
			tableContent += '<td>' + this.price + '</td>';
			tableContent += '<td>' + this.procent + '</td>';
			tableContent += '<td>' + this.price_our + '</td>';
			tableContent += '<td>' + this.wt + '</td>';
			tableContent += '<td>' + this.useriin + '</td>';
			tableContent += '<td>' + this.content + '</td>';
			tableContent += '<td>' + this.spcount + '</td>';
			tableContent += '<td>' + this.norm + '</td>';
			tableContent += '<td>' + this.code + '</td>';
			tableContent += '<td><img src="' + this.image + '" with="100"></td>';
			tableContent += '<td><a href="#" class="linkdeleteSP" rel="' + this._id + '"title="'+this.name+'">Удалить</a></td>';
			tableContent += '</tr>';
		});

		// Inject the whole content string into our existing HTML table
		$('#List table tbody').html(tableContent);
	});
};

// Add SP
function addSP(event) {
	event.preventDefault();

	// Super basic validation - increase errorCount variable if any fields are blank
	var errorCount = 0;
	$('#addSP input').each(function(index, val) {
		if($(this).val() === '') { errorCount++; }
	});

	// Check and make sure errorCount's still at zero
	if(errorCount === 0 ){


		// If it is, complite all user info into one object
		var newSP= {
			'name': $('#addSP fieldset input#inputSPName').val(),
			'price': $('#addSP fieldset input#inputSPPrice').val(),
			'procent': $('#addSP fieldset input#inputSPProcent').val(),
			'price_our': $('#addSP fieldset input#inputSPPriceOur').val(),
			'wt': $('#addSP fieldset input#inputSPWt').val(),
			'useriin': $('#addSP fieldset input#inputSPUserIin').val(),
			'content': $('#addSP fieldset textarea#inputSPContent').val(),
			'spcount': $('#addSP fieldset input#inputSPCount').val(),
			'norm': $('#addSP fieldset input#inputSPNorm').val(),
			'product': $('#addSP fieldset select#selectSPProduct').val(), 
			'image': $('#addSP fieldset input#inputSPImage').val(),
			'code': $('#addSP fieldset input#inputSPCode').val(),
		}
    
    // /User AJAX to post the object to our addSP service
    $.ajax({
    	type: 'POST',
    	data: newSP,
    	url: '/subproducts/addsp',
    	dataType: 'JSON'
    }).done(function( response ){

    	//Check for successful (blank) response
    	if (response.msg === ''){

    		//Clear the form inputs
    		$('#addSP fieldset input').val('');
    		$('#addSP fieldset textarea').val('');

    		//Update the table
    		showSP();
    	}
    	else {

    		//If something goes wrong, alert the error message that our service returned
    		alert('Error: ' + response.msg);
    	}
    });

	}
	else {

		//If errorCount is more than 0, error out
		alert('Все поля должны быть заполнены');
		return false;
	}
};

//Отображение продуктов
function showSelectProducts(){
  productContent='';
  $.getJSON('/catalogs/productlist', function (data){
    $.each(data, function(){
        productContent += "<option value='"+this._id+"'>"+this.name+"</option>";
      });
  $('#selectSPProduct').html(productContent);
  });
};



// Delete SP
function deleteSP(event) {
	
	event.defaultPrevented;

	// Pop up a confirmation dialog
	var confirmation = confirm('Вы уверены что хотите удалить продукт: '+$(this).attr('title')+'?');

	//Chek and make sure the user confirmed
	if (confirmation === true) {

		// If they did, do our delete
		$.ajax({
			type: 'DELETE',
			url: '/subproducts/deletesp/' + $(this).attr('rel')
		}).done(function( response ){

			//Chek for a successful (blank) response

			if (response.msg === ''){
			}
			else {
				alert('Error: ' + response.msg);
			}

			// Update the table
			showSP();
		});
	}
	else {

		//If they said no to the confirm, do nothing
		return false;
	}
}

// Show SP Info
function showSPInfo(event) {

    // Retrieve username from link rel attribute
	var SPId = $(this).attr('id');

	var getURL = '/subproducts/'+SPId;
  	var SPitem = [];
  	$.getJSON(getURL, function (data){
    	$('#inputSPName').val(data.name);
    	$('#inputSPId').val(data._id);
    	$('#inputSPPrice').val(data.price);
    	$('#inputSPProcent').val(data.procent);
    	$('#inputSPPriceOur').val(data.price_our);
    	$('#inputSPWt').val(data.wt);
    	$('#inputSPUserIin').val(data.useriin);
    	$('#inputSPContent').val(data.content);
    	$('#inputSPCount').val(data.spcount);
    	$('#inputSPNorm').val(data.norm);
    	$('#selectSPProduct').val(data.product);
    	$('#inputSPImage').val(data.image);
    	$('#inputSPCode').val(data.code);
    	$('#SPInfoImage').attr("src",data.image);
	});
    //Populate Info Box
};

$("#inputSPProcent").keyup(function(e){
	$("#inputSPPriceOur").val(parseFloat($('#inputSPPrice').val()*$(this).val()/100)+parseFloat($('#inputSPPrice').val()));	
})

function updateSP(event){
	event.preventDefault();
	
		// If it is, complite all user info into one object
		var updateSP= {
			'name': $('#addSP fieldset input#inputSPName').val(),
			'price': $('#addSP fieldset input#inputSPPrice').val(),
			'procent': $('#addSP fieldset input#inputSPProcent').val(),
			'price_our': $('#addSP fieldset input#inputSPPriceOur').val(),
			'wt': $('#addSP fieldset input#inputSPWt').val(),
			'useriin': $('#addSP fieldset input#inputSPUserIin').val(),
			'content': $('#addSP fieldset textarea#inputSPContent').val(),
			'spcount': $('#addSP fieldset input#inputSPCount').val(),
			'norm': $('#addSP fieldset input#inputSPNorm').val(),
			'product': $('#addSP fieldset select#selectSPProduct').val(), 
			'image': $('#addSP fieldset input#inputSPImage').val(),
			'code': parseFloat($('#addSP fieldset input#inputSPCode').val()),
		};
    
	var SPId =  $('#addSP fieldset input#inputSPId').val();

    // /User AJAX to post the object to our addSP service
    $.ajax({
    	type: 'PUT',
    	data: updateSP,
    	url: '/subproducts/update/'+SPId,
    	dataType: 'JSON'
    }).done(function( response ){

    	//Check for successful (blank) response
    	if (response.msg === ''){

    		//Clear the form inputs
    		$('#addSP fieldset input').val('');
    		$('#addSP fieldset textarea').val('');

    		//Update the table
    		showSP();
    	}
    	else {

    		//If something goes wrong, alert the error message that our service returned
    		alert('Error: ' + response.msg);
    	}
    });
};


function searchSP(){
    // Prevent Link from Firing

    // Retrieve username from link rel attribute
	var SPCode = $('#inputSPSerch').val();

	var getURL = '/subproducts/search/'+SPCode;
  	var SPitem = [];
  	$.getJSON(getURL, function (data){
    	$('#inputSPName').val(data.name);
    	$('#inputSPId').val(data._id);
    	$('#inputSPPrice').val(data.price);
    	$('#inputSPProcent').val(data.procent);
    	$('#inputSPPriceOur').val(data.price_our);
    	$('#inputSPWt').val(data.wt);
    	$('#inputSPUserIin').val(data.useriin);
    	$('#inputSPContent').val(data.content);
    	$('#inputSPCount').val(data.spcount);
    	$('#inputSPNorm').val(data.norm);
    	$('#selectSPProduct').val(data.product);
    	$('#inputSPImage').val(data.image);
    	$('#inputSPCode').val(data.code);
    	$('#SPInfoImage').attr("src",data.image);
	});
    //Populate Info Box
    $('#inputSPSerch').val('').focus();
};

function IncSkip(){
	SPScipList=SPScipList+10;
	showSP();
};

function decSkip(){
	if (SPScipList>0){
	SPScipList=SPScipList-10
	};
	showSP();
};




