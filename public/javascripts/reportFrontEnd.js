var sProducts = TAFFY();

// DOM Ready ==============================
$(document).ready(function(){
	showProductsTable();
});

//Fill table with data
function showProductsTable() {

	//Empty content string
	var tableContent = '';

	//jQuery AJAX call for JSON
	$.getJSON( '/reports/list', function( data ) {

 		// Stick our user data array into a userlist variable in the global object
        userListData = data;
        var sum = 0;
        var sumPriceOur = 0;
        var number = 1;
		
		//For each item in our JSON, add a table row and cells to the content string
		$.each(data, function(){
			tableContent += '<tr>';
			tableContent += '<td>' + number + '</td>';
			tableContent += '<td><a href="#" class="linkshowuser" rel="' + this.name + '" title="Show Details">' + this.name + '</a></td>';
			tableContent += '<td style="width:70%">' + new Date(this.datatime) + '</td>';
			tableContent += '<td>' + this.cheknum + '</td>';
			tableContent += '<td>' + this.useriin + '</td>';
			tableContent += '<td>' + this.price + '</td>';
			tableContent += '<td>' + this.price_our + '</td>';
			tableContent += '<td>' + this.count + '</td>';
			tableContent += '<td>' + ((parseFloat(this.price_our)-parseFloat(this.price))*parseFloat(this.count)) + '</td>';
			tableContent += '</tr>';
			number++;
			if (!isNaN(parseFloat(this.price_our)-parseFloat(this.price)))
			{
				if((parseFloat(this.price_our)-parseFloat(this.price))>0)
				sumPriceOur +=parseFloat(this.price_our)*parseFloat(this.count);
				sum +=(parseFloat(this.price_our)-parseFloat(this.price))*parseFloat(this.count);
			}
		});
			tableContent += '<tr><td>'+sumPriceOur+'</td><td>'+sum+'</td></tr>';

		// Inject the whole content string into our existing HTML table
		$('#List table tbody').html(tableContent);
	});
};
