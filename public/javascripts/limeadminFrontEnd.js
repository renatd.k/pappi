var Catalog = TAFFY();
var Subcatalog = TAFFY();
var Product = TAFFY();

// DOM Ready ==============================
$(document).ready(function(){
	showCatalogs();
	showSelectCatalog();
	showSelectSubcatalog();
	
	$('#Catalog').on('click', 'a.linkshowSC', showSCInfo);
	$('#Catalog').on('click', 'a.linkshowP', showPInfo);
	
	$('#addSC').on('click', 'button#btnAddSC',addSC);
	$('#addSC').on('click', 'button#btnUpdateSC',updateSC);
	$('#addSC').on('click', 'button#btnDeleteSC',deleteSC);

	$('#addP').on('click', 'button#btnAddP',addP);
	$('#addP').on('click', 'button#btnUpdateP',updateP);
	$('#addP').on('click', 'button#btnDeleteP',deleteP);

});


var contentSubcatalog = '';
var contentProduct = '';

var uicatalog='';


function showCatalogs(){
$.getJSON('/catalogs/subcataloglist', function (data){
	Subcatalog().remove();
	Subcatalog.insert(data);

	$.getJSON('/catalogs/productlist', function (data){
		Product().remove();
		Product.insert(data);
			Subcatalog().each(function(sitem){
				var sitem =sitem;
				uicatalog+="<li><span>"+sitem.name+"</span></li>"

				contentSubcatalog +="<li><b><a href='#' class='linkshowSC' id='"+sitem._id+"'>"+sitem.name+"</a></b>";
				contentSubcatalog +="<ul>";

				Product().each(function(pitem){
					if(pitem.subcatalog===sitem._id){
						contentProduct +="<li><a href='#' class='linkshowP' id='"+pitem._id+"'>"+pitem.name+"</a></li>";
					}
				});		
				contentSubcatalog +=contentProduct;
				contentProduct = "";
				contentSubcatalog +="</ul>";
				contentSubcatalog +="</li>"
			});
	 	$('#Catalog ul').html(contentSubcatalog);
	 	$('.easyui-tree li ul').html(uicatalog);
		contentSubcatalog = '';
		contentProduct = '';
	});
	});
};

//Отображение списка каталога
function showSelectCatalog(){
  catalogContent='';
  $.getJSON('/catalogs/cataloglist', function (data){
    $.each(data, function(){
        catalogContent += "<option value='"+this._id+"'>"+this.name+"</option>";
      });
  $('#selectSCCatalog').html(catalogContent);
  });
};

//Отображение списка подкаталога
function showSelectSubcatalog(){
  subcatalogContent='';
  $.getJSON('/catalogs/subcataloglist', function (data){
    $.each(data, function(){
        subcatalogContent += "<option value='"+this._id+"'>"+this.name+"</option>";
      });
  $('#selectPSubcatalog').html(subcatalogContent);
  });
};

// Show SC Info
function showSCInfo(event) {

    // Retrieve username from link rel attribute
	var SCId = $(this).attr('id');
	var SCItem=Subcatalog({_id:SCId}).first();

  	$('#inputSCId').val(SCItem._id);
  	$('#inputSCName').val(SCItem.name);
    $('#selectSCCatalog').val(SCItem.catalogs);
    //Populate Info Box
};

// Show P Info
function showPInfo(event) {

    // Retrieve username from link rel attribute
	var PId = $(this).attr('id');
	var PItem=Product({_id:PId}).first();
	var imageP="<img src='"+PItem.image+"'/>"

  	$('#inputPId').val(PItem._id);
  	$('#inputPName').val(PItem.name);
    $('#selectPSubcatalog').val(PItem.subcatalog);
    $('#inputPImage').val(PItem.image);
    $('#image').html(imageP);
    //Populate Info Box
};

// Add SC
function addSC(event) {
	event.preventDefault();

	// Super basic validation - increase errorCount variable if any fields are blank
	var errorCount = 0;
	$('#addSC input').each(function(index, val) {
		if($(this).val() === '') { errorCount++; }
	});

	// Check and make sure errorCount's still at zero
	if(errorCount === 0 ){

		var newSC= {
			'name': $('#addSC fieldset input#inputSCName').val(),
			'catalogs': $('#addSC fieldset select#selectSCCatalog').val(), 
		}
	    
	    // /User AJAX to post the object to our addSP service
	    $.ajax({
	    	type: 'POST',
	    	data: newSC,
	    	url: '/limeadmin/addsc',
	    	dataType: 'JSON'
	    }).done(function( response ){

	    	//Check for successful (blank) response
	    	if (response.msg === ''){

	    		//Clear the form inputs
	    		$('#addSC fieldset input').val('');

	    		//Update the table
	    		showCatalogs();
	    	}
	    	else {

	    		//If something goes wrong, alert the error message that our service returned
	    		alert('Error: ' + response.msg);
	    	}
	    });
	}
	else {

		//If errorCount is more than 0, error out
		alert('Все поля должны быть заполнены');
		return false;
	};
};

function updateSC(event){
	event.preventDefault();

		// If it is, complite all user info into one object
	var updateSC= {
			'name': $('#addSC fieldset input#inputSCName').val(),
			'catalogs': $('#addSC fieldset select#selectSCCatalog').val(), 
		};
    
	var SCId =  $('#addSC fieldset input#inputSCId').val();

    // /User AJAX to post the object to our addSP service
    $.ajax({
    	type: 'PUT',
    	data: updateSC,
    	url: '/limeadmin/updatesc/'+SCId,
    	dataType: 'JSON'
    }).done(function( response ){

    	//Check for successful (blank) response
    	if (response.msg === ''){

    		//Clear the form inputs
    		$('#addSC fieldset input').val('');
	
	   		showCatalogs();

    		//Update the table
    	}
    	else {

    		//If something goes wrong, alert the error message that our service returned
    		alert('Error: ' + response.msg);
    	}
    });
};

// Delete SC
function deleteSC(event) {
	
	event.defaultPrevented;

	// Pop up a confirmation dialog
	var confirmation = confirm('Вы уверены что хотите удалить продукт: '+$('#addSC fieldset input#inputSCName').val()+'?');

	//Chek and make sure the user confirmed
	if (confirmation === true) {

		// If they did, do our delete
		$.ajax({
			type: 'DELETE',
			url: '/limeadmin/deletesc/' + $('#addSC fieldset input#inputSCId').val()
		}).done(function( response ){

			//Chek for a successful (blank) response

			if (response.msg === ''){
			}
			else {
				alert('Error: ' + response.msg);
			}
	    	
	    	$('#addSC fieldset input').val('');

			// Update the table
			showCatalogs();
		});
	}
	else {

		//If they said no to the confirm, do nothing
		return false;
	};
};


// Add P
function addP(event) {
	event.preventDefault();

	// Super basic validation - increase errorCount variable if any fields are blank
	var errorCount = 0;
	$('#addP input').each(function(index, val) {
		if($(this).val() === '') { errorCount++; }
	});

	// Check and make sure errorCount's still at zero
	if(errorCount === 0 ){

		var newP= {
			'name': $('#addP fieldset input#inputPName').val(),
			'subcatalog': $('#addP fieldset select#selectPSubcatalog').val(), 
			'image': $('#addP fieldset input#inputPImage').val(), 
		}
	    
	    // /User AJAX to post the object to our addSP service
	    $.ajax({
	    	type: 'POST',
	    	data: newP,
	    	url: '/limeadmin/addp',
	    	dataType: 'JSON'
	    }).done(function( response ){

	    	//Check for successful (blank) response
	    	if (response.msg === ''){

	    		//Clear the form inputs
	    		$('#addP fieldset input').val('');

	    		//Update the table
	    		showCatalogs();
	    	}
	    	else {

	    		//If something goes wrong, alert the error message that our service returned
	    		alert('Error: ' + response.msg);
	    	}
	    });
	}
	else {

		//If errorCount is more than 0, error out
		alert('Все поля должны быть заполнены');
		return false;
	};
};

function updateP(event){
	event.preventDefault();

		// If it is, complite all user info into one object
	var updateP= {
			'name': $('#addP fieldset input#inputPName').val(),
			'subcatalog': $('#addP fieldset select#selectPSubcatalog').val(), 
			'image': $('#addP fieldset input#inputPImage').val(),
		};
    
	var PId =  $('#addP fieldset input#inputPId').val();

    // /User AJAX to post the object to our addSP service
    $.ajax({
    	type: 'PUT',
    	data: updateP,
    	url: '/limeadmin/updatep/'+PId,
    	dataType: 'JSON'
    }).done(function( response ){

    	//Check for successful (blank) response
    	if (response.msg === ''){

    		//Clear the form inputs
    		$('#addP fieldset input').val('');
	
	   		showCatalogs();

    		//Update the table
    	}
    	else {

    		//If something goes wrong, alert the error message that our service returned
    		alert('Error: ' + response.msg);
    	}
    });
};

// Delete SC
function deleteP(event) {
	
	event.defaultPrevented;

	// Pop up a confirmation dialog
	var confirmation = confirm('Вы уверены что хотите удалить продукт: '+$('#addP fieldset input#inputPName').val()+'?');

	//Chek and make sure the user confirmed
	if (confirmation === true) {

		// If they did, do our delete
		$.ajax({
			type: 'DELETE',
			url: '/limeadmin/deletep/' + $('#addP fieldset input#inputPId').val()
		}).done(function( response ){

			//Chek for a successful (blank) response

			if (response.msg === ''){
			}
			else {
				alert('Error: ' + response.msg);
			}
	    	
	    	$('#addP fieldset input').val('');

			// Update the table
			showCatalogs();
		});
	}
	else {

		//If they said no to the confirm, do nothing
		return false;
	};
};