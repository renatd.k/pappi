var Cart = TAFFY();

// DOM Ready ==============================
$(document).ready(function(){

  Cart.store("cookieCart");

  showCart();

  $('#inputName').val('').focus();

  
  	setSelectText();
  	setKeyUp();

	$('#List table tbody').on('click' , 'td a.deleteCartItem', deleteCartItem);
	$('#btnSendTiket').on('click','',sendTiket);

 });

function setSelectText(){
	$("input.small").focus(function () {
        $(this).select();
    }).mouseup(function(e){
        e.preventDefault();
    });	
};
	
function setKeyUp(){
	$("input.small").keyup(function(e){
		var cartItem=Cart({_id:$(this).attr('id')}).first();
	   	var itemSumSet="#s"+cartItem._id;
	   	var sumAllCartItem=0;
	   	
	   	cartItem.cartCount=$(this).val();
	   	cartItem.sum=cartItem.cartCount*cartItem.price_our;
		$(itemSumSet).text(cartItem.sum);	


		Cart().map(function(record,recordnumber){
		 	return sumAllCartItem +=parseFloat(record.sum);
		});

		Cart(cartItem).update({cartCount:parseFloat(cartItem.cartCount)});


		$("#summa").text(sumAllCartItem);
	 });	
}	
	

function showCart(){
	var tableContent = '';
	var sumCart = 0;
	Cart().each(function(item){
		tableContent += '<tr>';
		tableContent += '<td> <img height="65px" src="' + item.image + '"/></td>';
		tableContent += '<td>' + item.name + '</td>';
		tableContent += '<td>' + item.wt + '</td>';
		tableContent += '<td>' + item.code + '</td>';
		tableContent += '<td>' + item.price_our + '</td>';
		tableContent += '<td><input class="small" type="text" id="'+item._id+'"value="' + item.cartCount + '"/></td>';
		tableContent += '<td id="s'+item._id+'">' + item.sum + '</td>';
		tableContent += '<td><a href="#" class="btn red deleteCartItem" rel="' + item._id + '"title="'+item.name+'">X</a></td>';
		tableContent += '</tr>';
		sumCart = sumCart + parseFloat(item.sum);
	});
    
	tableContent += '<tr>';
	tableContent += '<td></td><td></td><td></td><td></td><td></td><td></td><td><h1 id="summa">' + sumCart + '</h1></td><td></td>';
	tableContent += '</tr>';

// Inject the whole content string into our existing HTML table
	$('#List table tbody').html(tableContent);
};

function deleteCartItem(e){
	e.preventDefault();

	Cart({_id:$(this).attr('rel')}).remove();

	showCart();
  	setKeyUp();
  	setSelectText();
}

function sendTiket(){
    var errorCount = 0;

	$('#userInfo input').each(function(index, val) {
		if($(this).val() === '') { errorCount++; }
	});


	// Check and make sure errorCount's still at zero
	if(errorCount === 0 ){

	    var cartAll = Cart().get();
	    var sumAllCartItem = 0;

		Cart().map(function(record,recordnumber){
		 	return sumAllCartItem +=parseFloat(record.sum);
		});

	    // If it is, complite all user info into one object
		var allData= {
			'name': $('#userInfo input#inputName').val(),
			'street': $('#userInfo input#inputStreet').val(),
			'Phone': $('#userInfo input#inputPhone').val(),
			'email': $('#userInfo input#inputEmail').val(),
			'comment': $('#userInfo textarea#inputComment').val(),
			'sum': sumAllCartItem,
		};

		$.ajax({
	    	type: 'POST',
	    	data: {cartAll:cartAll,allData:allData},
	    	url: '/cart/sendmail',
	    	dataType: 'JSON'
	    }).done(function( response ){

	    	//Check for successful (blank) response
	    	if (response.msg === ''){

	    		alert("Заявка отправлена "+allData.name+"!");
	    		Cart().remove();
				window.location.href = "/";
	    	}
	    	else {

	    		//If something goes wrong, alert the error message that our service returned
	    		alert('Error: ' + response.msg);
	    	}
	    });
	}
	else
	{
		//If errorCount is more than 0, error out
		alert('Все поля со (*) должны быть заполнены!');
		return false;
	}
}