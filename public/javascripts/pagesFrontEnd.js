var url;

function newUser(){
	$('#dlg').dialog('open').dialog('setTitle','Новая страница');
	$('#fm').form('clear');
	url = '/pages/create';
}

function editUser(){
	var row = $('#dg').datagrid('getSelected');
	
	if (row){
		$('#dlg').dialog('open').dialog('setTitle','Редактирование страницы');
		$('#fm').form('load',row);
		url = '/pages/update/'+row._id;
	}
}

function saveUser(){
	$('#fm').form('submit',{
		url: url,
		onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(result){
			var result = eval('('+result+')');
			if (result.errorMsg){
				$.messager.show({
				title: 'Error',
				msg: result.errorMsg
			});
			} else {
				$('#dlg').dialog('close'); // close the dialog
				$('#dg').datagrid('reload'); // reload the user data
			}
		}
	});
}

function destroyUser(){
var row = $('#dg').datagrid('getSelected');
	
	if (row){
		$.messager.confirm('Confirm','Are you sure you want to destroy this user?',function(r){
			if (r){
				$.post('/pages/destroy',{id:row._id},function(result){
					if (result.success){
						$('#dg').datagrid('reload'); // reload the user data
					} else {
						$.messager.show({ // show error message
							title: 'Error',
							msg: result.errorMsg
						});
					}
				},'json');
			}
		});
	}
}