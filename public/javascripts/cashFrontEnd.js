var Cart = TAFFY();
var chekNum = 0;
// DOM Ready ==============================
$(document).ready(function(){
	$('#btnSerchSP').on('click', searchSP);
	$('#cartToDb').on('click', cartToDb);
    $('<audio id="alertAudio"><source src="song/rington4ik.info_sms_no1_2011.mp3" type="audio/mpeg"></audio>').appendTo('body');
    $('#List table tbody').on('click' , 'td a.linkdeleteSP', removeCashItem);
	getChekNum()
});

function getChekNum(){
	var getURL = '/cash/getchek';
    $.getJSON(getURL, function (data){
    	if(data[0].cheknum){
    	chekNum = parseFloat(data[0].cheknum)+1;
		}
		else{
			chekNum = 1;
		}
	    $('#chek').text("#"+chekNum);
	    $('#inputSPSerch').val('').focus();

	});
}

$(document).keypress(function(e) {
    if(e.which == 13) {
		searchSP();
    };
});

function removeCashItem(event){
	event.preventDefault();
	var productId = $(this).attr('rel');
    var getURL = '/subproducts/'+productId;
    $.getJSON(getURL, function (data){
		Cart(data).remove();
		showCart();
	    $('#inputSPSerch').val('').focus();

	});
};

function searchSP(){
    // Prevent Link from Firingfirst_money?utm_source=e-mail&utm_medium=kanal4&utm_campaign=kanal4

    // Retrieve username from link rel attribute
	var SPCode = $('#inputSPSerch').val();
	var inputCount = $('#inputCount').val();

	if( SPCode === ""){
    	$('#inputSPSerch').val('').focus();
	}
		else
	{
		var getURL = '/cash/search/'+SPCode;
		//jQuery AJAX call for JSON
	  	$.getJSON(getURL, function (data){

	  	if (data === null){
	  		$('#alertAudio')[0].play();
	  		alert('Нет в базе');
	  	}
	  	else
	  	{
	 		if (Cart(data).count()>0){
	 		 	Cart(data).update(function(){
				    if(inputCount===""){this.count++;}else{this.count +=parseFloat(inputCount)};
				    this.sum=this.count*this.price_our;
				    return this;
  				});
	 		}
	 		else{
		        Cart.insert(data);
		        if(inputCount===""){Cart(data).update({count:1})}else{Cart(data).update({count:parseFloat(inputCount)})};
		        Cart(data).update(function(){
		        	this.sum=this.price_our*this.count;
		        	return this;
		        });
	 		}
				
			showCart();
			$('#inputCount').val("");

		};
	  	});
	    //Focus to insert
	    $('#inputSPSerch').val('').focus();
	};
};

function showCart(){
	var tableContent = '';
	var sumCart = 0;
	Cart().each(function(item){
		tableContent += '<tr>';
		tableContent += '<td>' + item.name + '</td>';
		tableContent += '<td>' + item.wt + '</td>';
		tableContent += '<td> <img width="65px" src="' + item.image + '"/></td>';
		tableContent += '<td>' + item.price_our + '</td>';
		tableContent += '<td>' + item.count + '</td>';
		tableContent += '<td>' + item.sum + '</td>';
		tableContent += '<td><a href="#" class="linkdeleteSP" rel="' + item._id + '"title="'+item.name+'">Удалить</a></td>';
		tableContent += '</tr>';
		sumCart = sumCart + parseFloat(item.sum);
	});
    
	tableContent += '<tr>';
	tableContent += '<td></td><td></td><td></td><td></td><td></td><td><h1>' + sumCart + '</h1></td><td></td>';
	tableContent += '</tr>';

// Inject the whole content string into our existing HTML table
	$('#List table tbody').html(tableContent);
};

function cartToDb(){

	var cartData = {};
	var d = new Date();
	var dataNow = d.toISOString();


	Cart().each(function(item){
		cartData={"price":item.price,"code":item.code,"name":item.name,"useriin":item.useriin,"sp_id":item._id,"spcount":parseFloat(item.spcount)-parseFloat(item.count),"count":item.count,"price_our":item.price_our,"sum":item.sum,"datatime":dataNow,"cheknum":chekNum};

	    $.ajax({
	    	type: 'POST',
	    	data: cartData,
	    	url: '/cash/addcart/',
	    	dataType: 'JSON'
	    }).done(function( response ){

	    	//Check for successful (blank) response
	    	if (response.msg === ''){
	    	}
	    	else {
	    		//If something goes wrong, alert the error message that our service returned
	    		alert('Error: ' + response.msg);
	    	}
	    });

	    $.ajax({
	    	type: 'POST',
	    	data: cartData,
	    	url: '/cash/decrsp/',
	    	dataType: 'JSON'
	    }).done(function( response ){
			
			//Check for successful (blank) response
	    	if (response.msg === ''){
	    	}
	    	else {
	    		//If something goes wrong, alert the error message that our service returned
	    		alert('Error: ' + response.msg);
	    	}
	    });
	});

	Cart().remove();
	showCart();
	getChekNum();
	$('#inputSPSerch').val('').focus();

};
