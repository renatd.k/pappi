var Cart = TAFFY();

$(document).ready(function(){

 Cart.store("cookieCart");

 showCatalog();

  $('#cataloglist ul').on('click', 'li a', showSubcatalogs);
  $('#subcataloglist ul').on('click', 'li a', showProducts);
  $('#productlist ul').on('click', 'li a', showSubproducts);
  $('#subproductlist ul').on('click', 'li a', showSubproduct);
  $('#cartpage').on('click', showCart);
  $('#subproduct').on('click','a', addToCart);

});

function showCatalog(){
  $.mobile.loading( "show", {theme: true});
  $.getJSON('/catalogs/cataloglist', function (data){
    
    var listcatalog = "";

    $.each(data, function(){
        listcatalog += '<li><a href="#subcatalogpage" data-transition="slide" id='+this._id+'>'+this.name;
        listcatalog += '</a></li>';
      });

      // Inject the whole content string into our existing HTML table
      $('#cataloglist ul').html(listcatalog);
      $('#cataloglist ul').listview('refresh');
      $.mobile.loading( "hide" );
    });
};

//Отображение продуктов
function showSubcatalogs(event){
   // alert($(this).attr('id'));
  // $.mobile.changePage("#subcatalogpage");
  $.mobile.loading( "show", {theme: true});
  var subcatalogContent = "";
  var catalogid ="";
  catalogid = $(this).attr("id");
  
  $.getJSON('/catalogs/subcatalog/'+catalogid, function (data){
    $.each(data, function(){
          subcatalogContent += '<li href="#bar" ><a href="#productpage" data-transition="slide" id='+this._id+'>'+this.name;
          subcatalogContent += "</a></li>";  
    });
   $('#subcataloglist ul').html(subcatalogContent);
   $('#subcataloglist ul').listview('refresh');
   $.mobile.loading( "hide" );
  });
};

//Отображение продуктов
function showProducts(event){
  // $.mobile.changePage("#productpage");
  $.mobile.loading( "show", {theme: true});
  var productContent = "";
  var subcatalogid ="";
  subcatalogid = $(this).attr("id");
  
  $.getJSON('/catalogs/productlist/'+subcatalogid, function (data){
    $.each(data, function(){
          productContent += '<li href="#bar" ><a href="#subproductspage" data-transition="slide" id='+this._id+'>'+this.name;
          productContent += "</a></li>";  
    });
   $('#productlist ul').html(productContent);
   $('#productlist ul').listview('refresh');
   $.mobile.loading( "hide" );
  });
};

//Отображение подпродуктов
function showSubproducts(event){
  // $.mobile.changePage("#subproductspage");
  $.mobile.loading( "show", {theme: true});
  var subproductsContent = "";
  var productid ="";
  productid = $(this).attr("id");
  $.getJSON('/catalogs/subproductlist/'+productid, function (data){
    $.each(data, function(){
          subproductsContent += '<li href="#bar" ><a href="#subproductpage" data-transition="slide" id='+this._id+'>'+this.name;
          subproductsContent += "</a></li>";  
    });
   $('#subproductlist ul').html(subproductsContent);
   $('#subproductlist ul').listview('refresh');
   $.mobile.loading( "hide" );
  });
};

//Отображение подпродуктов
function showSubproduct(event){
  // $.mobile.changePage("#subproductpage");
  $.mobile.loading( "show", {theme: true});
  var subproductContent = "";
  var subproductid ="";
  subproductid = $(this).attr("id");
  $.getJSON('/subproducts/'+subproductid, function (data){
          subproductContent += '<h3>'+data.name+"</h3>";
          subproductContent += '<h4> Объем: '+data.wt+"</h4>";
          subproductContent += '<h4> Цена: '+data.price_our+" тг.</h4>";
          subproductContent += '<p><img src='+data.image+" ></p>";
          subproductContent += '<p><img src='+data.content+" ></p>";
          subproductContent += '<h4> Остаток на скдале: '+data.spcount+"</h4>";
          subproductContent += '<a href="#" class="ui-btn ui-icon-action ui-btn-icon-bottom" id='+data._id+'> Отправить в Корзину</a>';
   $('#subproduct').html(subproductContent);
   $.mobile.loading( "hide" );
  });
};

function addToCart(event){
  event.preventDefault();

 var getURL = '/subproducts/'+$(this).attr('id');
 
 $.getJSON(getURL, function (data){
    
    if (Cart(data).count()>0){
        Cart(data).update(function(){
            this.cartCount++;
            this.sum=this.cartCount*this.price_our;
            return this;
          });
      }
      else{
            Cart.insert(data);
            Cart(data).update({cartCount:1});
            Cart(data).update({sum:data.price_our});
      }
    showCart();
 });
};

function showCart(){
  $.mobile.loading( "show", {theme: true});
  var cartContent = "";

  Cart().each(function(record, recordnumber){
    cartContent += '<li href="#bar" ><a data-transition="slide" id='+record._id+'>'+record.name;
    cartContent += "</a></li>";
  });
  
  $('#subproductincart ul').html(cartContent);
  $('#subproductincart ul').listview('refresh');
  $.mobile.loading( "hide" );
}


