// DOM Ready ==============================
$(document).ready(function(){
  showCatalogs();
});

function showCatalogs(){
 catalogContent='';
  $.getJSON('/catalogs/cataloglist', function (data){
    $.each(data, function(){
        catalogContent += "<li><span>"+this.name+"</span></li>";
      });
  $('.easyui-tree ul').html(catalogContent);
  });
};