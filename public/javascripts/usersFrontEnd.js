// Userlist data array for filling in info box
var userListData = [];

// DOM Ready =================================
$(document).ready(function(){

	// Populate the user table on initial page load
	userProductTable();

	//Username link click
	$('#List table tbody').on('click', 'td a.linkshowuser', showUserInfo);

	// Add User button click
	$('#btnAddUser').on('click', addUser);

	// Delete User link click
	$('#List table tbody').on('click' , 'td a.linkdeleteuser', deleteUser);

});

//Functions ==================================

//Fill table with data
function userProductTable() {

	//Empty content string
	var tableContent = '';

	//jQuery AJAX call for JSON
	$.getJSON( '/subproducts/userproducts/1', function( data ) {

 		// Stick our user data array into a userlist variable in the global object
        userListData = data;
		var sumPrice=0;
		var sumPriceOur=0;
		var itemNum=1;
		//For each item in our JSON, add a table row and cells to the content string
		$.each(data, function(){
			tableContent += '<tr>';
			tableContent += '<td>'+itemNum+'</td>';
			tableContent += '<td><a href="#" class="linkshowuser" rel="' + this.name + '" title="Show Details">' + this.name + '</a></td>';
			tableContent += '<td>' + this.code + '</td>';
			tableContent += '<td>' + this.spcount + '</td>';
			tableContent += '<td>' + this.price + '</td>';
			tableContent += '<td>' + this.price_our + '</td>';
			tableContent += '<td><a href="#" class="linkdeleteuser" rel="' + this._id + '">delete</a></td>';
			tableContent += '</tr>';
			sumPrice +=parseFloat(this.price)*parseFloat(this.spcount);
			sumPriceOur +=parseFloat(this.price_our);
			itemNum++;
		});
		tableContent +='<tr><td>'+sumPrice+'</td><td>'+sumPriceOur+'</td><td>'+(sumPriceOur-sumPrice)+'</td></tr>'

		// Inject the whole content string into our existing HTML table
		$('#List table tbody').html(tableContent);
	});
};

// Show User Info
function showUserInfo(event) {

    // Prevent Link from Firing
    event.defaultPrevented;

    // Retrieve username from link rel attribute
    var thisUserName = $(this).attr('rel');


    // Get Index of object based on id value
    var arrayPosition = userListData.map(function(arrayItem) { return arrayItem.username; }).indexOf(thisUserName);

    // Get our User Object
    var thisUserObject = userListData[arrayPosition];

    //Populate Info Box
    $('#userInfoName').text(thisUserObject.fullname);
    $('#userInfoAge').text(thisUserObject.age);
    $('#userInfoGender').text(thisUserObject.gender);
    $('#userInfoLocation').text(thisUserObject.location);
};

// Add User
function addUser(event) {
	event.defaultPrevented;

	// Super basic validation - increase errorCount variable if any fields are blank
	var errorCount = 0;
	$('#addUser input').each(function(index, val) {
		if($(this).val() === '') { errorCount++; }
	});

	// Check and make sure errorCount's still at zero
	if(errorCount === 0 ){

		// If it is, complite all user info into one object
		var newUser= {
			'username': $('#addUser fieldset input#inputUserName').val(),
			'email': $('#addUser fieldset input#inputUserEmail').val(),
			'fullname': $('#addUser fieldset input#inputUserFullname').val(),
			'age': $('#addUser fieldset input#inputUserAge').val(),
			'location': $('#addUser fieldset input#inputUserLocation').val(),
			'gender': $('#addUser fieldset input#inputUserGender').val()
		}
    
    // /User AJAX to post the object to our adduser service
    $.ajax({
    	type: 'POST',
    	data: newUser,
    	url: '/users/adduser',
    	dataType: 'JSON'
    }).done(function( response ){

    	//Check for successful (blank) response
    	if (response.msg === ''){

    		//Clear the form inputs
    		$('#addUser fieldset input').val('');

    		//Update the table
    		populateTable();
    	}
    	else {

    		//If something goes wrong, alert the error message that our service returned
    		alert('Error: ' + response.msg);
    	}
    });

	}
	else {

		//If errorCount is more than 0, error out
		alert('Please fill in all fields');
		return false;
	}
};


// Delete user
function deleteUser(event) {
	
	event.defaultPrevented;
	// Pop up a confirmation dialog
	var confirmation = confirm('Are you sure you want to delete this user?');

	//Chek and make sure the user confirmed
	if (confirmation === true) {

		// If they did, do our delete
		$.ajax({
			type: 'DELETE',
			url: '/users/deleteuser/' + $(this).attr('rel')
		}).done(function( response ){

			//Chek for a successful (blank) response

			if (response.msg === ''){
			}
			else {
				alert('Error: ' + response.msg);
			}

			// Update the table
			populateTable();
		});
	}
	else {

		//If they said no to the confirm, do nothing
		return false;
	}
}