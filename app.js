var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mailer = require('express-mailer');

var passport = require('passport');
var flash    = require('connect-flash');
var session = require('express-session');

var routes = require('./routes/index');
var users = require('./routes/users');
var catalogs = require('./routes/catalogs');
var subproducts = require('./routes/subproducts');
var cash = require('./routes/cash');
var cart = require('./routes/cart');
var limeadmin = require('./routes/limeadmin');
var reports = require('./routes/reports');
var suppliers = require('./routes/suppliers');
var pages = require('./routes/pages');
var mobile = require('./routes/mobile');

var app = express();

// Database
var mongo = require('mongoskin');
var db = mongo.db("mongodb://renat:MONGOLAB1985renat@ds059938.mongolab.com:59938/heroku_app27230650", {native_parser:true});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch', cookie: { maxAge: 24 * 60 * 60 * 1000 } })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


// Make our db accessible to our router
app.use(function(req,res,next){
    req.db = db;
    next();
});

app.use(function(req,res,next){
    req.locals=app.locals;
    next();
});

// uncomment this line
require('./config/passport')(passport); // pass passport for configuration

// routes ======================================================================
require('./routes/passport_routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// maler settings
mailer.extend(app, {
  from: 'renat@limonchik.kz',
  host: 'smtp.gmail.com', // hostnameuserlist
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: 'renatd.k@gmail.com',
    pass: 'GMAIL2008Alimzhan'
  }
});

app.use(function(req, res, next){
    req.mailer = app.mailer;
    next();
})

app.use('/', routes);
app.use('/users', users);
app.use('/catalogs', catalogs);
app.use('/subproducts',subproducts);
app.use('/cash',cash);
app.use('/cart',cart);
app.use('/reports',reports);
app.use('/limeadmin',limeadmin);
app.use('/suppliers',suppliers);
app.use('/pages',pages);
app.use('/mobile',mobile);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers



// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
