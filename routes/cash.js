var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID

/* GET home page. */
router.get('/', function(req, res) {
  res.render('cash/index', { title: 'Касса' });
});


router.get('/search/:code',function(req,res){
	var db = req.db;
	db.collection('subproducts').findOne({code:parseFloat(req.params.code)}, function(err, result){
		res.json(result);
	});
});

router.post('/addcart',function(req, res){
	var db = req.db;
	req.body.datatime = new Date(req.body.datatime);
	db.collection('carts').insert(req.body, function(err, reslut){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

router.post('/decrsp',function(req, res){
	var db = req.db;
	db.collection('subproducts').update({_id:ObjectID(req.body.sp_id)},{$set:{"spcount":req.body.spcount}},function(err,result){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

router.get('/getchek', function(req, res){
	var db = req.db;
	db.collection('carts').find().sort({_id:-1}).limit(1).skip(parseInt(req.params.spskip)).toArray(function (err, items){
    	res.json(items);
    });
});

module.exports = router;