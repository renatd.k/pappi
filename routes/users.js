var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID


/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Мини-маркет Лимончик' });
});

/* GET home page. */
router.get('/user', function(req, res) {
  res.render('user/index', { user : req.user });
});

/* GET user by Id */
router.get('/:id', function(req, res) {
  var userId=req.params.id;
  var db = req.db;
  db.collection('userlist').findOne({_id:ObjectID(userId)},function(err, items){
		res.json(items);
});
});

/* GET users listing. */
router.get('/userlist', function(req, res) {
	var db = req.db;
	db.collection('userlist').find().toArray(function (err, items){
		res.json(items);
	});
});

/* POST to adduser */
router.post('/adduser', function(req, res) {
	var db = req.db;
	db.collection('userlist').insert(req.body, function(err, reslut){
		res.send(
			(err === null) ? { msg: ''} : { msg: err } 
			);
	});
});

/* DELETE  to deleteuser*/
router.delete('/deleteuser/:id', function(req, res) {
	var db = req.db;
	var userToDelete = req.params.id;
	db.collection('userlist').removeById(userToDelete, function(err, result) {
		res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
	});
});

/* GET home page. */
router.get('/editprofile', function(req, res) {
	consile.logs("editprofile");
  res.send("editprofile");
});


module.exports = router;
