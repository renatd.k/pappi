var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('mobile/index', { title: 'Papi.kz интернет-маркет' });
});

/* GET home page. */
router.get('/clear', function(req, res) {
  res.render('mobile/index_clear', { title: 'Papi.kz интернет-маркет' });
});



module.exports = router;
