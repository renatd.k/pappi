var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID

/* GET home page. */
router.get('/', function(req, res) {
  res.render('pages/index', { title: 'Papi.kz интернет-маркет' });
});


/* GET home page. */
router.get('/crud', function(req, res) {
  res.render('pages/crud', { title: 'Papi.kz интернет-маркет' });
});

/*Get item dy id*/
router.get('/:id',function(req, res) {
	var db = req.db;

	db.collection('pages').find().limit(5).sort({_id:-1}).toArray(function (err, items){
    	var items=items;
		var SPID = req.params.id;
		db.collection('pages').findOne({_id:ObjectID(SPID)},function(err, item){
	  		res.render('pages/index', { title: 'Papi.kz интернет-маркет', item:item, items:items});
		});
    });
	
});

/* POST Catalog List */
router.post('/list',function(req, res){
	var db = req.db;
    db.collection('pages').find().toArray(function (err, items){
    	res.json(items);
    });
});

/* POST to page*/
router.post('/create',function(req, res){
	var db = req.db;
	db.collection('pages').insert(req.body, function(err, reslut){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

/*Update page*/
router.post('/update/:id',function(req,res) {
	var db = req.db;
	db.collection('pages').update({_id:ObjectID(req.params.id)},{$set:req.body},function(err,result){
		res.send((result === 1) ? { success: 1 } : { errorMsg:'error: ' + err });
	});
});
/* DELETE  to deleteSP*/
router.post('/destroy', function(req, res) {
	var db = req.db;
	var spToDelete = req.body.id;
	db.collection('pages').removeById(ObjectID(spToDelete), function(err, result) {
		res.send((result === 1) ? { success: 1 } : { errorMsg:'error: ' + err });
	});
});

module.exports = router;