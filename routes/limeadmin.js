var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID

/* GET home page. */
router.get('/', function(req, res) {
  res.render('limeadmin/index', { title: 'Papi.kz интернет-маркет' });
});

/* GET home page. */
router.get('/ui', function(req, res) {
  res.render('limeadmin/ui_index', { title: 'Papi.kz интернет-маркет' });
});

/* POST to addsc*/
router.post('/addsc',function(req, res){
	var db = req.db;
	db.collection('subcatalogs').insert(req.body, function(err, reslut){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

/*Update SC*/
router.put('/updatesc/:id',function(req,res) {
	var db = req.db;
	db.collection('subcatalogs').update({_id:ObjectID(req.params.id)},{$set:req.body},function(err,result){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

/* DELETE  deleteSC*/
router.delete('/deletesc/:id', function(req, res) {
	var db = req.db;
	db.collection('subcatalogs').removeById(req.params.id, function(err, result) {
		res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
	});
});

/* POST to addp*/
router.post('/addp',function(req, res){
	var db = req.db;
	db.collection('products').insert(req.body, function(err, reslut){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

/*Update P*/
router.put('/updatep/:id',function(req,res) {
	var db = req.db;
	db.collection('products').update({_id:ObjectID(req.params.id)},{$set:req.body},function(err,result){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

/* DELETE  deleteP*/
router.delete('/deletep/:id', function(req, res) {
	var db = req.db;
	db.collection('products').removeById(req.params.id, function(err, result) {
		res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
	});
});

module.exports = router;
