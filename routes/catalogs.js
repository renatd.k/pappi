var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID

/* GET home page. */
router.get('/', function(req, res) {
    var db = req.db;
    db.collection('pages').find().limit(5).sort({_id:-1}).toArray(function (err, items){
        res.render('catalogs/index', { title: 'Papi.kz интернет-маркет', items:items });
    });
});

/* GET Catalog List */
router.get('/cataloglist',function(req, res){
    var db = req.db;
    db.collection('catalogs').find().toArray(function (err, items){
        res.json(items);
    });
});

/* POST Catalog List */
router.post('/list',function(req, res){
    var db = req.db;
    db.collection('catalogs').find().toArray(function (err, items){
        res.json(items);
    });
});

/*GET Subcatalog List*/
router.get('/subcataloglist', function(req, res){
    var db = req.db;
    db.collection('subcatalogs').find().sort({name:1}).toArray(function (err, items){
        res.json(items);
    });
});

/*GET Subcatalog List*/
router.get('/subcatalog/:catalogid', function(req, res){
    var db = req.db;
    db.collection('subcatalogs').find({catalogs:req.params.catalogid}).sort({name:1}).toArray(function (err, items){
        res.json(items);
    });
});

/*GET Product List*/
router.get('/productlist', function(req, res){
    var db = req.db;
    db.collection('products').find().sort({name:1}).toArray(function (err, items){
        res.json(items);
    });
});

/*GET Product List*/
router.get('/productlist/:subcatalogid', function(req, res){
    var db = req.db;
    db.collection('products').find({subcatalog:req.params.subcatalogid}).sort({name:1}).toArray(function (err, items){
        res.json(items);
    });
});


/*GET Subproducts List*/
router.get('/subproductlist/:id', function(req, res){
    var db = req.db;
    db.collection('subproducts').find({'product': ObjectID(req.params.id),}).toArray(function (err, items){
        res.json(items)
    })
})

module.exports = router;
