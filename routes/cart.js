var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID

/* GET home page. */
router.get('/', function(req, res) {
  res.render('cart/index', { title: 'Корзина' });
});


/*Send Mail to ...*/
router.post('/sendmail',function(req, res, next){
	var mailer = req.mailer;
	var locals = req.locals;
	locals.cart = req.body.cartAll;
	locals.allData = req.body.allData;
	locals.email = req.body.allData.email;
    mailer.send({
    template:'email',
    bcc: 'renatd.k@gmail.com',
	}, 
	{
    to: locals.email, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
    subject: 'Заявка оформлена!', // REQUIRED.
    otherProperty: 'Other Property' // All additional properties are also passed to the template as local variables.
  }, function (err) {
	  	res.send(
				(err === null) ? { msg: ''} : {msg: err}
				);
  });
});


module.exports = router;
