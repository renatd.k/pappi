var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID


/* GET home page. */
router.get('/', function(req, res) {
  res.render('subproducts/index', { title: 'Подпродукты' });
});

router.get('/list/:spskip',function(req, res){
	var db = req.db;
	db.collection('subproducts').find().limit(10).skip(parseInt(req.params.spskip)).toArray(function (err, items){
    	res.json(items);
    });
});
router.get('/all',function(req, res){
	var db = req.db;
	db.collection('subproducts').find({product: { $exists: true } }).toArray(function (err, items){
    	res.json(items);
    });
});



/* POST to addsp*/
router.post('/addsp',function(req, res){
	var db = req.db;
	req.body.product = ObjectID(req.body.product);
	req.body.code = parseFloat(req.body.code);
	db.collection('subproducts').insert(req.body, function(err, reslut){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});

});


/* DELETE  to deleteSP*/
router.delete('/deletesp/:id', function(req, res) {
	var db = req.db;
	var spToDelete = req.params.id;
	db.collection('subproducts').removeById(spToDelete, function(err, result) {
		res.send((result === 1) ? { msg: '' } : { msg:'error: ' + err });
	});
});

/*Get item dy id*/
router.get('/:id',function(req, res) {
	var db = req.db;
	var SPID = req.params.id;
	db.collection('subproducts').findOne({_id:ObjectID(SPID)},function(err, items){
		res.json(items);
	});
});

router.get('/search/:code',function(req,res){
	var db = req.db;
	db.collection('subproducts').findOne({code:parseFloat(req.params.code)}, function(err, result){
		res.json(result);
	});
});

/*Update item*/
router.put('/update/:id',function(req,res) {
	var db = req.db;
	req.body.product = ObjectID(req.body.product);
	req.body.code = parseFloat(req.body.code);
	db.collection('subproducts').update({_id:ObjectID(req.params.id)},{$set:req.body},function(err,result){
		res.send(
			(err === null) ? { msg: ''} : {msg: err}
			);
	});
});

router.get('/userproducts/:spskip',function(req, res){
	var db = req.db;
	db.collection('subproducts').find({useriin:"810905300861"}).limit(4000).skip(parseInt(req.params.spskip)).toArray(function (err, items){
    	res.json(items);
    });
});


module.exports = router;