var express = require('express');
var router = express.Router();
var ObjectID = require('mongoskin').ObjectID


/* GET home page. */
router.get('/', function(req, res) {
  res.render('reports/index', { title: 'Отчеты' });
});


/* GET Preport List */
router.get('/list',function(req, res){
	var db = req.db;
    db.collection('carts').find({datatime:{"$gte":new Date(2014,7,26)}}).sort({datatime:-1}).toArray(function (err, items){
    	res.json(items);
    });
});


module.exports = router;